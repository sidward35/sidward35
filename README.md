# [Siddharth Mathur](https://sidward35.gitlab.io)

## Recent Projects

[MiniViz - Live treemap visual of the S&P 500](http://m-viz.gitlab.io)

[Letterboxd + Justwatch](https://gitlab.com/sidward35/letterboxd-justwatch) - ⭐ 7 Stars, [See it in action here](https://js.smathur.xyz)

[Sherlock Username Search](https://gitlab.com/sidward35/sherlock-web) - ⭐ 1 Star

## Top Projects

### Mobile Apps

[FoodCyclr](https://gitlab.com/sidward35/FoodCyclr) - 🏆 [Hackathon Winner](https://devpost.com/software/foodcyclr), 🍴 1 Fork

[PCPartPicker](https://gitlab.com/sidward35/PCPartPicker) - 👨‍💼 Acknowledged by the CEO of PCPartPicker, LLC, ⭐ 6 Stars, 🍴 2 Forks

### IoT

[HumiraHelper](https://gitlab.com/sidward35/HumiraHelper) - 🏆 [Hackathon Winner](https://twitter.com/abbvie/status/1145693699118174208)

[Deauth/Rickroll AP](https://gitlab.com/sidward35/Deauth-RickRollAP) - ⭐ 12 Stars, 🍴 4 Forks

[ESP8266 Smoke Alarm](https://gitlab.com/sidward35/esp8266-smoke-alarm) - ⭐ 1 Star

### Stock Market

[Splunk App for Stock Market Analysis](https://gitlab.com/sidward35/splunk-stocks-analysis) - ⭐ 3 Stars, 🍴 1 Fork

[Trade Stocks with Alexa/Google Home](https://gitlab.com/sidward35/VoiceTrader) - 🍴 1 Fork

[DJIA Rankings with Pandas](https://gitlab.com/sidward35/djia-rankings)

### Other

[Apprisen 2.0](https://gitlab.com/sidward35/Apprisen2-0) - 🏆 [Hackathon Winner](https://twitter.com/Apprisen/status/1185649343870582784)

[Visualizing US Spending with Python](https://gitlab.com/sidward35/usa-spending)

[Marvle (Marvel-themed Wordle)](https://gitlab.com/sidward35/marvle)

[Chipotle (Chipotle-themed Wordle)](https://gitlab.com/sidward35/chipotle)

## Other Content

[Tech Multireddit](https://sidward35.gitlab.io/tech)

[Finance Multireddit](https://sidward35.gitlab.io/money)

<script src="https://cdn.signalfx.com/o11y-gdi-rum/latest/splunk-otel-web.js" crossorigin="anonymous">
</script>
<script>
    SplunkRum.init({
        beaconUrl: 'https://rum-ingest.us1.signalfx.com/v1/rum',
        rumAuth: 'JUeo4BsZeSWJ8QA7RKV2yA',
        app: 'personal-website'
    });
</script>
